const autoprefixer = require("autoprefixer");
const sass = require("sass");

module.exports = [{
        mode: "development",
        entry: ["./app.scss"],
        output: {
            filename: "bundle.js",
        },
        module: {
            rules: [{
                    test: /\.scss$/,
                    use: [{
                            loader: "file-loader",
                            options: {
                                name: "bundle.css",
                            },
                        }, {
                            loader: "extract-loader"
                        }, {
                            loader: "css-loader"
                        }, {
                            loader: "postcss-loader",
                            options: {
                                postcssOptions: {
                                    plugins: [autoprefixer()],
                                },
                            },
                        }, {
                            loader: "sass-loader",
                            options: {
                                implementation: sass,
                                webpackImporter: false,
                                sassOptions: {
                                    includePaths: ["./node_modules"],
                                },
                            },
                        },
                    ],
                },
            ],
        },
    },
];
