require("dotenv").config();

const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const apiRoutes = require('./api.js');
const mongoose = require("mongoose");
const passport = require('passport');
const expressSession = require("express-session");
const MongoStore = require("connect-mongo");
const Gambler = require("./Gambler.js");

const PORT = 3220;
const isProduction = process.env.MODE == "production";

const morganMiddleware = morgan("dev");
const bodyParserJsonMiddleware = bodyParser.json({
    extended: false,
});
const bodyParserMiddleware = bodyParser.urlencoded({
    extended: false
});

let secret = "secret";
let mongoUrl = isProduction ? `mongodb://2217220:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/2217220?authSource=admin` : "mongodb://127.0.0.1:27017/blackjack";
let store = MongoStore.create({
    mongoUrl: mongoUrl
});
let sessionMiddleware = expressSession({
    secret: secret,
    resave: false,
    saveUninitialized: false,
    store: store,
});

app.set('view engine', 'ejs');

app.use(sessionMiddleware);

app.use(passport.initialize());
app.use(passport.session());
app.use(
    morganMiddleware,
    bodyParserJsonMiddleware,
    bodyParserMiddleware);

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated())
        return next();
    return res.redirect("/");
}

const serveTablePage = (req, res) => {
    res.sendFile(path.join(__dirname, "./public/table.html"));
};
app.get("/table", ensureAuthenticated, serveTablePage);
app.get("/table.html", ensureAuthenticated, serveTablePage);
app.get('/profile', ensureAuthenticated, async (req, res) => {
  try {
    const searchedGambler = await Gambler.findById(req.user.id);
    const objGambler = searchedGambler.toObject();
    res.render('pages/profile', {
        username: objGambler.username,
        fullName: objGambler.fullName,
    });
  } catch (err) {
    console.error(err);
    res.sendStatus(400);
  }
});
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "./public/index.html"));
});

app.use(express.static("public"), express.static("dist"));

app.use("/api", apiRoutes);

async function main() {
    if (isProduction) {
        await mongoose.connect(`mongodb://192.168.171.67:27017/2217220`, {
            useNewUrlParser: true,
            authSource: "admin",
            user: "2217220",
            pass: process.env.MONGO_PASSWORD,
        });
    } else {
        await mongoose.connect("mongodb://127.0.0.1:27017/blackjack");
    }

    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}`);
    });
}

main().catch((err) => console.error(err));
