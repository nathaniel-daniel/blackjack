const express = require("express");

const Gambler = require("./Gambler.js");
const passport = require("passport");
const LocalStrategy = require("passport-local");

const router = express.Router();

router.get("/user/:id", async(req, res) => {
    try {
        const searchedGambler = await Gambler.findById(req.params.id);
        const objGambler = searchedGambler.toObject();

        delete objGambler.password;

        res.json(objGambler);
    } catch (err) {
        console.error(err);
        res.sendStatus(400);
    }
});

router.get("/user/:id/hash", async(req, res) => {
    try {
        const searchedGambler = await Gambler.findById(req.params.id);
        const objGambler = searchedGambler.toObject();

        res.json({
            hash: objGambler.password
        });
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

router.post("/user/", async(req, res) => {
    try {
        const newGambler = await Gambler({
            ...req.body
        });

        await newGambler.save();

        res.json({
            id: newGambler._id
        });
    } catch (err) {
        console.error(err);
        res.sendStatus(400);
    }
});

passport.use(
    new LocalStrategy(async function verify(username, password, cb) {
        try {
            const foundUser = await Gambler.findOne({
                username: username
            });
            if (!foundUser) {
                return cb(null, false, {
                    message: "Incorrect username or password."
                });
            }
            foundUser.validatePassword(password, cb);
        } catch (err) {
            return cb(err);
        }
    }));

passport.serializeUser(function (user, cb) {
    process.nextTick(function () {
        cb(null, {
            id: user._id,
            username: user.username
        });
    });
});

passport.deserializeUser(function (user, cb) {
    process.nextTick(function () {
        return cb(null, user);
    });
});

router.post(
    "/login/password",
    passport.authenticate("local", {
        successRedirect: "/table",
        failureRedirect: "/",
    }));

module.exports = router;
